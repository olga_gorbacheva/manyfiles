package ru.god.manyfiles;

import java.io.*;

/**
 * Класс, осуществляющий чтение данных из текстового файла и их запись в виде целых чисел в файл intdata.dat
 *
 * @author Горбачева, 16ИТ18к
 */
public class CreateFileOfInt {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\intdata.txt"));
             DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("src\\intdata.dat"))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                outputStream.writeInt(Integer.valueOf(string));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}