package ru.god.manyfiles;

import java.io.*;

/**
 * Класс, предназначенный для чтения целых чисел из файла, отбора чисел больше 999 и их записи в файл intnewdata.dat
 *
 * @author Горбачева, 16ИТ18к
 */
public class WriteCorrectNumbers {
    public static void main(String[] args) {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("src\\intdata.dat"));
             DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("src\\intnewdata.dat"))) {
            int number;
            while (inputStream.available() > 0) {
                number = inputStream.readInt();
                if ((number < 1_000_000) && (number > 999)) {
                    outputStream.writeInt(number);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}