package ru.god.manyfiles;

import java.io.*;

/**
 * Класс, позволяющий отобрать "счастливые билеты" и записать их в файл lucky.txt
 *
 * @author Горбачева, 16ИТ18к
 */
public class FindLuckyTicket {
    public static void main(String[] args) {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("src\\intnewdata.dat"));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\lucky.txt"))) {
            String string;
            while (inputStream.available() > 0) {
                string = correctForm(inputStream.readInt());
                if (isLucky(string)) {
                    bufferedWriter.write(string + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, позволяющий определить, является ли число номером счастливого билета
     * для чисел в диапазоне от 1_000 до 999_999
     *
     * @param string - номер билета
     * @return true, если билет счастливый, false в любом другом случае
     */
    private static boolean isLucky(String string) {
        char[] array = string.toCharArray();
        return array[0] + array[1] + array[2] == array[3] + array[4] + array[5];
    }

    /**
     * Метод, позволяющий преобразовать числа с числом знаков менее 6 к нужному виду
     *
     * @param number - исходное число
     * @return число в корректной форме
     */
    private static String correctForm(int number) {
        StringBuilder string = new StringBuilder();
        int length = String.valueOf(number).length();
        for (int i = 0; i < 6 - length; i++) {
            string.append("0");
        }
        string.append(String.valueOf(number));
        return string.toString();
    }
}